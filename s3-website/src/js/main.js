
import '../scss/main.scss';
var $ = require('jquery');
require('bootstrap')
window.jQuery = $;
window.$ = $;

$(document).ready(() => {
    $("button:lt(5)").each((i, feelingButton) => {
        $(feelingButton).on("click", () => {
            let alertTag = $("#feedback-alert");
            $('#feedback-alert span').last().html(`Your vote "<strong>${$(feelingButton).attr('class')}</strong>" has been saved!`);
            alertTag.css('display', 'inline-block').hide().fadeIn("slow");
            $([document.documentElement, document.body]).animate({
                scrollTop: alertTag.offset().top
            }, 1000);
            setTimeout(() => alertTag.fadeOut("slow"), 5000);
        })
    })

});