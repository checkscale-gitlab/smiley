terraform {
  required_providers {
    aws = {
      version = "~> 3.0"
      source  = "hashicorp/aws"
    }
  }
}

provider "aws" {
  default_tags {
    tags = {
      app = "smiley"
    }
  }
}

provider "aws" {
  alias = "east"
  region = "us-east-1"
  default_tags {
    tags = {
      app = "smiley"
    }
  }
}

terraform {
  backend "http" {}
}

locals {
  common-tags = {
    env = "global"
  }
}


data "aws_region" "current" {}
data "aws_caller_identity" "current" {}
