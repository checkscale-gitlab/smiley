resource "aws_api_gateway_rest_api" "smiley-apigw" {
  name = "smiley-global-apigw"

  tags = local.common-tags

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_resource" "api-resource" {
  rest_api_id = aws_api_gateway_rest_api.smiley-apigw.id
  parent_id   = aws_api_gateway_rest_api.smiley-apigw.root_resource_id
  path_part   = "api"
}

resource "aws_api_gateway_method" "submit-feeling-method" {
  rest_api_id   = aws_api_gateway_rest_api.smiley-apigw.id
  resource_id   = aws_api_gateway_resource.api-resource.id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "feeling-lambda-integration" {
  rest_api_id = aws_api_gateway_rest_api.smiley-apigw.id
  resource_id = aws_api_gateway_method.submit-feeling-method.resource_id
  http_method = aws_api_gateway_method.submit-feeling-method.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = "arn:aws:apigateway:${data.aws_region.current.name}:lambda:path/2015-03-31/functions/arn:aws:lambda:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:function:$${stageVariables.feelingSubmitLambdaArn}/invocations"
  
}
