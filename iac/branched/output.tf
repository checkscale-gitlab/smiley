output "website-endpoint" {
  value = aws_s3_bucket_website_configuration.smiley-bucket-config.website_endpoint
}

output "apigateway-api-endpoint" {
  value = aws_api_gateway_deployment.stage-deploy.invoke_url
}

output "cloudfront-distribution-domain" {
  value = aws_cloudfront_distribution.smiley-distribution.domain_name
}