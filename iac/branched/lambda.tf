

data "archive_file" "submit-feeling-lambda-zip" {
  type        = "zip"
  source_file = "../../lambdas/submit_feeling/main.py"
  output_path = "dist/submit_feeling_lambda.zip"
}


resource "aws_lambda_function" "submit-feeling-lambda" {
  function_name = "smiley-submit-feeling-${var.environment}"

  s3_bucket = var.global-bucket-name
  s3_key    = "${var.environment}/lambda/submit_feeling.zip"

  handler = "main.lambda_handler"
  runtime = "python3.8"

  role = aws_iam_role.submit-feeling-lambda-role.arn

  tags = local.common-tags
}

resource "aws_lambda_function_url" "submit-feeling-lambda-url" {
  function_name      = aws_lambda_function.submit-feeling-lambda.function_name
  authorization_type = "AWS_IAM"
}

resource "aws_iam_role" "submit-feeling-lambda-role" {
  name               = "smiley-submit-feeling-lambda-role-${var.environment}"
  tags               = local.common-tags
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "basic-lambda-execution-policy" {
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  role       = aws_iam_role.submit-feeling-lambda-role.name
}

